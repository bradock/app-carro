import 'package:flutter/material.dart';

class Contato extends StatefulWidget {
  @override
  _ContatoState createState() => _ContatoState();
}

class _ContatoState extends State<Contato> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "AUTOMÓVEIS",
          style: TextStyle(
              color: Colors.black87,
              fontWeight: FontWeight.bold,
              fontFamily: 'Pacifico',
              fontSize: 16),
        ),
        centerTitle: true,
        backgroundColor: Colors.white,
        actions: <Widget>[
          Padding(
              padding: EdgeInsets.only(right: 20),
              child: GestureDetector(
                onTap: (){
                  Navigator.pop(context);
                },
                child: Icon(
                Icons.home,
                color: Colors.black87,
                size: 26,
              )))
        ],
      ),
      body: Stack(children: [
        Positioned(
          right: MediaQuery.of(context).size.width/9,
          top: 100,
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text("Para realizar o orçamento entre em contasto:",),
                Divider(),
                Text("Telefone: (61) 9.2313-2312", style: TextStyle(fontSize: 25),textAlign: TextAlign.center,),
                Divider(),
                Text("Email: email@email.com", style: TextStyle(fontSize: 20))
              ]),
        ),
        Positioned(
          bottom: 0,
          left: MediaQuery.of(context).size.width/2.5,
          child: Padding(
            padding: EdgeInsets.all(6),
            child: Text("Automóveis"),
          ),
          )
      ]),
    );
  }
}
