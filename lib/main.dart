import "package:flutter/material.dart";
import "package:car/contato.dart";

void main() {
  runApp(MaterialApp(
    title: "App para vendas de automóveis",
    home: Home(),
  ));
}

class Home extends StatefulWidget {
  @override
  _State createState() => _State();
}

class _State extends State<Home> {

  bool modeloSUV = false;
  bool modeloEsport = true;
  bool modeloPicape = true;
  @override
  Widget build(BuildContext context) {
    //print("$carros");
    return (Scaffold(
        appBar: AppBar(
          title: Text(
            "AUTOMÓVEIS",
            style: TextStyle(
                color: Colors.black87,
                fontWeight: FontWeight.bold,
                fontFamily: 'Pacifico',
                fontSize: 16),
          ),
          centerTitle: true,
          backgroundColor: Colors.white,
          actions: <Widget>[
            Padding(
                padding: EdgeInsets.only(right: 20),
                child: GestureDetector(
                  onTap: () {
                    Navigator.push((context),
                        new MaterialPageRoute(builder: (context) => Contato()));
                  },
                  child: Icon(
                    Icons.contact_phone,
                    color: Colors.black87,
                    size: 26,
                  ),
                ))
          ],
        ),
        body: Column(crossAxisAlignment: CrossAxisAlignment.start, children: <
            Widget>[
          Padding(
            padding: EdgeInsets.only(top:14, left: 12),
            child: Text(
              "Categorias",
              style: TextStyle(
                fontWeight: FontWeight.normal,
                fontSize: 18,
                fontFamily: 'Pacifico',
              ),
            ),
          ),
          Expanded(
            flex: 1,
            child:
                ListView(scrollDirection: Axis.horizontal, children: <Widget>[
              menuCategoria("images/car13.jpg", "Picape", "Picape"),
              menuCategoria("images/car4.jpg", "Esportivo", "Esport"),
              menuCategoria("images/car1.jpg", "SUV", "SUV"),
            ]),
          ),
          Expanded(
            flex: 3,
            child: Padding(
                padding: EdgeInsets.only(top: 9, left: 0),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(left: 14),
                        child: Text("Carros Disponíveis - Listagem",
                            style: TextStyle(
                              fontWeight: FontWeight.normal,
                              fontSize: 18,
                              fontFamily: 'Pacifico',
                            )),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 10),
                        child: SizedBox(
                          height: MediaQuery.of(context).size.height / 2.1,
                          child: SingleChildScrollView(
                            child: Column(children: <Widget>[
                              menuCarros("images/car2.jpg", "Clio 2.0", "Esport", "2020", "0", modeloEsport),
                              menuCarros("images/car3.jpg", "Fiat 2.0", "Esport", "2019", "25225", modeloEsport),
                              menuCarros("images/car7.jpg", "Gol 2.0", "Esport", "2018", "15454", modeloEsport),

                              menuCarros("images/car5.jpg", "HRV 4.0", "SUV", "2020", "0", modeloSUV),
                              menuCarros("images/car8.jpg", "HRV 2.0", "SUV", "2019", "26555", modeloSUV),
                              menuCarros("images/car9.jpg", "Ranger", "SUV", "2018", "25580", modeloSUV),

                              menuCarros("images/car11.jpg", "Toro 4.4", "Picap", "2018", "3025", modeloPicape),
                              menuCarros("images/car12.jpg", "Toro", "Picap", "2018", "124410", modeloPicape),
                              menuCarros("images/car10.jpg", "Hillux", "Picap", "2020", "0", modeloPicape),
                              
                            ],),
                          ),
                        ),
                      )
                    ])),
          ),
        ])));
  }

  Widget menuCategoria(String img, String nome, String modelo) {
    return Padding(
        padding: EdgeInsets.all(10),
        child: GestureDetector(
          onTap: () {
            setState(() {
              if(modelo == "Esport"){
                  modeloEsport = true;
                  modeloSUV = false;
                  modeloPicape = false;
              }
              
              if(modelo == "SUV"){
                  modeloEsport = false;
                  modeloSUV = true;
                  modeloPicape = false;
              }

              if(modelo == "Picape"){
                  modeloEsport = false;
                  modeloSUV = false;
                  modeloPicape = true;
              }
             
            });
          },
          child: Stack(
            children: <Widget>[
              Container(
                width: 160,
                height: 115,
                color: Colors.blueGrey,
                child: Column(
                  children: <Widget>[
                    Image.asset(
                      "$img",
                      height: 88,
                      width: 160,
                    ),
                    Text(
                      "$nome",
                      style: TextStyle(
                        color: Colors.white,
                        fontFamily: 'Pacifico',
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ));
  }

  Widget menuCarros(
      String img, String nome, String modelo, String ano, String km, bool visi) {
    return Visibility(
        visible: visi,
        child: Padding(
          padding: EdgeInsets.all(10),
          child: Container(
            width: MediaQuery.of(context).size.width,
            height: 180,
            decoration: BoxDecoration(
                color: Colors.blueGrey.withOpacity(0.1),
                borderRadius: BorderRadius.circular(6)),
            child: Row(
              children: <Widget>[
                Image.asset(
                  "$img",
                  height: 160,
                ),
                Padding(
                  padding: EdgeInsets.only(top: 10),
                  child: SizedBox(
                    width: MediaQuery.of(context).size.width / 3.8,
                    height: 130,
                    child: Column(
                      children: <Widget>[
                        Text(
                          "$nome",
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        Text("Modelo: $modelo"),
                        Text("Ano: $ano"),
                        Text("KM: $km"),
                        FlatButton(
                          child: Text(
                            "Orçamento",
                            style: TextStyle(color: Colors.white, fontSize: 11),
                          ),
                          onPressed: () {
                            Navigator.push(
                                (context),
                                new MaterialPageRoute(
                                    builder: (context) => Contato()));
                          },
                          color: Colors.blueGrey,
                        )
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ));
  }
}
